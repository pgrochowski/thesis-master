\select@language {english}
\contentsline {chapter}{Introduction}{3}{chapter*.2}
\contentsline {chapter}{\numberline {1}Preliminaries}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Gaussian states}{7}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Entanglement of the bipartite system}{8}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}Unruh effect}{9}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Free scalar field in a curved spacetime}{10}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Rindler coordinates}{13}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Minkowski and Rindler modes}{14}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Unruh temperature -- 1+1-dimensional example}{16}{subsection.1.2.4}
\contentsline {chapter}{\numberline {2}Entanglement as an observer-dependent quantity}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Early attempts}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}Entanglement of global modes}{18}{section.2.2}
\contentsline {section}{\numberline {2.3}Localized projective measurement of a quantum field}{24}{section.2.3}
\contentsline {chapter}{\numberline {3}Effect of relativistic acceleration on localized two-mode Gaussian states in $3+1$-dimensional spacetime}{27}{chapter.3}
\contentsline {section}{\numberline {3.1}Modified Rindler coordinates}{27}{section.3.1}
\contentsline {section}{\numberline {3.2}The channel}{29}{section.3.2}
\contentsline {section}{\numberline {3.3}Derivation of the channel}{31}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Antiparallel accelerations, no separation}{33}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Antiparallel accelerations, nonzero separation}{34}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Parallel accelerations}{36}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}$a$-independence of the channel}{36}{subsection.3.3.4}
\contentsline {section}{\numberline {3.4}Choice of the modes}{37}{section.3.4}
\contentsline {section}{\numberline {3.5}Entanglement of the vacuum}{39}{section.3.5}
\contentsline {section}{\numberline {3.6}Concluding remarks}{41}{section.3.6}
\contentsline {chapter}{\numberline {4}Skew oriented accelerating observers}{43}{chapter.4}
\contentsline {section}{\numberline {4.1}The setup}{43}{section.4.1}
\contentsline {section}{\numberline {4.2}Characterization of the quantum channel}{44}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Calculating the noise matrix $N$}{46}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}The choice of modes}{47}{section.4.3}
\contentsline {section}{\numberline {4.4}Entanglement of the vacuum}{48}{section.4.4}
\contentsline {chapter}{\numberline {5}Conclusions and outlook}{51}{chapter.5}
\contentsline {chapter}{Appendix \numberline {A}Calculation of the cross elements in the noise matrix for the skew observers}{53}{Appendix.a.A}
\contentsline {chapter}{Bibliography}{57}{chapter*.19}
